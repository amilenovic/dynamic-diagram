# Dynamic Diagram (Dygram)

## What it is?
Project Dygram allows users to interactively explore a diagram representing SW project. Unlike UML, where model is static and where we need multiple diagrams (Class, Sequence, Use Case, etc diagrams), with Dygram user starts with predefined scope, but after he or she is able to explore anything what is of interest.

The author of the dygram is specifying it in JSON format.

## Architecture
Let's use this [Dygram](https://bitbucket.org/amilenovic/dynamic-diagram/raw/master/public_html/index.html) to describe itself.
