/* 
 * (c) Biformatic
 * Aleksandar Milenovic
 */

DispConnection.NORMAL   = "normal";
DispConnection.SELECTED = "selected";
DispConnection.IN_FLOW  = "inflow";

DispConnection.CON_REF   = "conRef";

// Default SVG Styles
DispConnection.DefaultStyle = {
    path : {
        normal   : {"stroke":"lightgray", "stroke-width":"5", "cursor":"pointer"}, 
        selected : {"stroke":"green", "stroke-width":"5", "cursor":"pointer"},
        inflow   : {"stroke":"green", "stroke-width":"5", "cursor":"pointer"}
    }
};


function DispConnection( fromDispEn, toDispEn, paper, style, onClick) {
    this.fromDispEn = fromDispEn;
    this.toDispEn   = toDispEn;
    this.onClickFwd = onClick;
    this.paper = paper;
    
    this.style = style != null ? style : DispConnection.DefaultStyle;
    deepAttrsMerge(this.style, DispConnection.DefaultStyle);
    
    this.state = DispConnection.NORMAL;
    this.prevState = null;
    
    this.path = null;
    this.label = null;
}

DispConnection.prototype.toString = function() {
    return "[" + this.fromDispEn + "->" + this.toDispEn + " DispConnection]";
};

DispConnection.prototype.prepareForDisplay = function( pathStr ) {
    with(this) {
        var startP = fromDispEn.getDepOutPoint();
        var endP   = toDispEn.getDepInPoint();
        
        if ( pathStr == null )
           pathStr = [ "M", startP.x, startP.y, "L", endP.x, endP.y ];
        
        path = paper.path(pathStr);
        var attrs = style.path[state];
//        addDescription(attrs);
        path.attr(attrs); //.shadow();
        path.data(DispConnection.CON_REF, this);
        attachListeners(path, onClickFwd);
    }
};

DispConnection.prototype.attachListeners = function(obj, onClickForward) {
    var dc = this;
    var onHoverIn = function() {
        var hEn = this.data(DispConnection.CON_REF);
        hEn.path.animate({"stroke": "lightgreen"}, 10);
        hEn.path.toFront();
    },
    
    onHoverOut = function() {
        var hEn = this.data(DispConnection.CON_REF);
        hEn.path.animate(hEn.style.path[hEn.state], 10);   
    },
            
    onClick = function() {
        onClickForward.call(dc, dc.paper);
    };
    
    obj.click(onClick);
    obj.hover(onHoverIn, onHoverOut);
};

DispConnection.prototype.updateLabel = function(state) {
    
};

DispConnection.prototype.saveState = function() {
    this.prevState = this.state;
};

DispConnection.prototype.restoreState = function() {
    if ( this.prevState != null ) {
        this.state = this.prevState;
        this.prevState = null;
    }
    this.setState(this.state);
};

DispConnection.prototype.setState = function(state) {
    this.state = state;
    this.path.attr(this.style.path[state]); //addDescription(path);
    this.path.toFront();
};
