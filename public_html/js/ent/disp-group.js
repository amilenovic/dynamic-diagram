/* 
 * (c) Biformatic
 * Aleksandar Milenovic
 */

// Global constants
DispGroup.ENTITIES = "entities";
DispGroup.PORTS = "ports";
DispGroup.DEFAULT_PAD = { x: 40, y: 40 };

function DispGroup( en, entities, map, paper, style, onClick ) {
    
    this.superclass(en, paper, style, onClick);

    this.grid = null;
    this.dispEntities = [];
    
    // constraction of the group will result in whole object graph to be created.
    for( var i = 0, ii = entities.length; i < ii; i++ ) {
        var chE = entities[i], dispE;
        
        if ( chE.hasOwnProperty( DispGroup.ENTITIES ) ) {
            // fix this later
            if ( style != null && style.hasOwnProperty(chE.type) && style[chE.type].hasOwnProperty("drawingClass") && style[chE.type].drawingClass == "SmallGroup" ) {
                console.log("Construct SmallGroup");
                dispE = new SmallGroup( chE, chE.entities, map, this.paper, style, onClick );
            }
            else 
                dispE = new DispGroup( chE, chE.entities, map, this.paper, style, onClick );
        }
        else if ( chE.hasOwnProperty( DispGroup.PORTS ) ) {
            dispE = new DispEntWithPorts( chE, map, this.paper, style, onClick );
        }
        else {
           dispE = new DispEntity( chE, this.paper, style, onClick );
//           dispE = new SmallEntity( chE, this.paper, style, onClick );
        }
        this.dispEntities.push(dispE);
        map.addDispEntity(dispE, this);
//        console.log(dispE);
    }
}

DispGroup.prototype = new DispEntity();
DispGroup.prototype.superclass = DispEntity;
DispGroup.prototype.constructor = DispGroup;

DispGroup.prototype.toString = function() {
    var name = this.en == null ? "GRID" : this.en.eId;
    return "[" + name + " DispGroup]";
};

DispGroup.prototype.prepareForDisplay = function(x, y) {   
    var offset = { x : this.en == null ? 0 : DispGroup.DEFAULT_PAD.x,
                   y : this.en == null ? 0 : DispEntity.DEFAULT_BOX_HEIGHT + DispGroup.DEFAULT_PAD.y
    };

    this.grid = new GridCalc(0, 0, DispGroup.DEFAULT_PAD.x, DispGroup.DEFAULT_PAD.y);
    this.grid.startX = x + offset.x;
    this.grid.startY = y + offset.y;
    
    for( var i = 0, ii = this.dispEntities.length; i < ii; i++ ) {
        var dispE = this.dispEntities[i];
        var box = dispE.prepareForDisplay(this.grid.startX, this.grid.startY);
        this.grid.addBox( box, dispE.en.level, dispE.en.position);
    }   

    var b = this.grid.getTotalGridSize();
    this.box.x = x; this.box.y = y;
    this.box.width = b.width + 2*DispGroup.DEFAULT_PAD.x; 
    this.box.height += b.height + 2*DispGroup.DEFAULT_PAD.y;
 
    if ( this.en != null ) {
       this.superclass.prototype.prepareForDisplay.call(this, x, y); 
    }
    
    this.repositionEntity(x, y);
    return this.box;
};

DispGroup.prototype.repositionEntity = function(x, y) {
    this.superclass.prototype.repositionEntity.call(this, x, y);
    var offset = { x : this.en == null ? 0 : DispGroup.DEFAULT_PAD.x,
                   y : this.en == null ? 0 : DispEntity.DEFAULT_BOX_HEIGHT + DispGroup.DEFAULT_PAD.y
    };
    this.grid.startX = x + offset.x;
    this.grid.startY = y + offset.y;
    
    for( var i = 0, ii = this.dispEntities.length; i < ii; i++ ) {
        var dispE = this.dispEntities[i];
        var coords = this.grid.getCoords(dispE.en.level, dispE.en.position);
        dispE.repositionEntity(coords.x, coords.y);
    }
};

DispGroup.prototype.getPathSegmentOut = function(list, dispConn) { 
    var de = list.pop();
    if ( de != null )
       return this.grid.getPathSegmentOut(de.getDepOutPoint(), this.getDepOutPoint());
    
    return "";
};

DispGroup.prototype.getPathSegmentIn = function(list, dispConn) { 
    var de = list.pop();
    if ( de != null )
       return this.grid.getPathSegmentIn(this.getDepInPoint(), de.getDepInPoint(list) ); 
   
    return "";
};