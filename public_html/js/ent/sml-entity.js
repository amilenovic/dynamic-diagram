/* 
 * (c) Biformatic
 * Aleksandar Milenovic
 */

SmallEntity.HEIGHT = Dygram.u;
SmallEntity.WIDTH  = 6.5 * Dygram.u;

// Default SVG Styles
SmallEntity.DefaultStyle = {
    shape : {
        normal   : { shadow : false },
        selected : { shadow : false },
        inflow   : { shadow : false }
    },
    text  : {
        normal   : { "font-size" : "11", "text-anchor" : "start" }, 
        selected : { "font-size" : "11", "text-anchor" : "start" },
        hoverover : { "font-size" : "11" },
        inflow   : { "font-size" : "11", "text-anchor" : "start" }
    }
};

function SmallEntity(en, paper, style, onClick) {
    if ( style != null ) {
       if( style.hasOwnProperty(en.type) ) 
          deepAttrsMerge(style[en.type], SmallEntity.DefaultStyle);
       else
          style[en.type] = SmallEntity.DefaultStyle;
    }  
    this.superclass(en, paper, style, onClick);
    this.box.width  = SmallEntity.WIDTH;
    this.box.height = SmallEntity.HEIGHT;
    this.textOffset.x = this.u;
    this.textOffset.y = this.u / 2;
    this.iconOffset.width = 12;
    this.iconOffset.x = -6;
    this.iconOffset.y = -6;
 }

SmallEntity.prototype = new DispEntity();
SmallEntity.prototype.superclass = DispEntity;
SmallEntity.prototype.constructor = SmallEntity;

SmallEntity.prototype.toString = function() {
    var name = this.en == null ? "GRID" : this.en.eId;
    return "[" + name + " SmallEntity]";
};

SmallEntity.prototype.prepareText = function() {  
    with(this) {
        var n = en.hasOwnProperty("name") ? en.name : en.eId;
        text = paper.text(box.x + textOffset.x, box.y + textOffset.y, n);
        var attrs = style.text[state];
        text.attr(attrs);
        addDescription(text);

        if ( !canFit(text, shape.getBBox(), n) )
           cutOff(text, shape.getBBox());

        text.data(DispEntity.EN_REF, this);    
        attachListeners(text, onClickFwd);
    }
};


