/* 
 * (c) Biformatic
 * Aleksandar Milenovic
 */

function DispEntWithPorts( en, map, paper, style, onClick ) {
    this.superclass(en, paper, style, onClick);

    if ( ! en.ports.hasOwnProperty("length") )
        this.en.ports = [en.ports];
    
     this.dispPorts = [];
    for( var i = 0, ii = this.en.ports.length; i < ii; i++ ) {
        var p = new Port( en.ports[i], paper, style, this.onClickFwd);
        map.addDispEntity(p, this);
        this.dispPorts.push(p);
    }
    this.portBox = new Box(0, 0, this.box.width, this.box.height + Port.HEIGHT);
}

DispEntWithPorts.prototype = new DispEntity();
DispEntWithPorts.prototype.superclass = DispEntity;
DispEntWithPorts.prototype.constructor = DispEntWithPorts;

DispEntWithPorts.prototype.toString = function() {
    var name = this.en == null ? "GRID" : this.en.eId;
    return "[" + name + " ports " + this.dispPorts.length + " " + this.portBox + " DispEntWithPorts]";
};

DispEntWithPorts.prototype.prepareForDisplay = function(x, y) {  
    with(this) {
        portBox.x = x; portBox.y = y;
        superclass.prototype.prepareForDisplay.call(this, x, y + Port.HEIGHT); 
        
        var gap = ~~ ( box.width / (dispPorts.length + 1) );
        
        for( var i = 0, ii = dispPorts.length; i < ii; i++ ) {
            dispPorts[i].box.width = gap;
            dispPorts[i].prepareForDisplay(x + gap*(i+1),y);

        }

        return portBox;
    }
};

DispEntWithPorts.prototype.repositionEntity = function(x, y) {
    with(this) {
        portBox.x = x; portBox.y = y;
        superclass.prototype.repositionEntity.call(this, x, y + Port.HEIGHT);
        
        var gap = ~~ ( box.width / (dispPorts.length + 1) );
        for( var i = 0, ii = dispPorts.length; i < ii; i++ ) {
            dispPorts[i].repositionEntity(x + gap*(i+1),y);
        }
    }
};

DispEntWithPorts.prototype.getDepInPoint = function(list) {
    if ( list != null && list.hasOwnProperty("length") && list.length > 0 ) {
        // make end point directly to the port.
        var port = list.pop();
        return port.getDepInPoint(list);
    }
    
    return { x : this.portBox.x + this.portBox.width / 2,
             y : this.portBox.y };
};
