/* 
 * (c) Biformatic
 * Aleksandar Milenovic
 */

// Global Constants
DispEntity.NORMAL   = "normal";
DispEntity.SELECTED = "selected";
DispEntity.IN_FLOW  = "inflow";
DispEntity.HOVER_OVER  = "hoverover";

DispEntity.EN_REF   = "enRef";
DispEntity.DEFAULT_BOX_HEIGHT = 2*Dygram.u;
DispEntity.DEFAULT_BOX_WIDTH  = 8*Dygram.u;
DispEntity.DEFAULT_ICON = 'M31.229,17.736c0.064-0.571,0.104-1.148,0.104-1.736s-0.04-1.166-0.104-1.737l-4.377-1.557c-0.218-0.716-0.504-1.401-0.851-2.05l1.993-4.192c-0.725-0.91-1.549-1.734-2.458-2.459l-4.193,1.994c-0.647-0.347-1.334-0.632-2.049-0.849l-1.558-4.378C17.165,0.708,16.588,0.667,16,0.667s-1.166,0.041-1.737,0.105L12.707,5.15c-0.716,0.217-1.401,0.502-2.05,0.849L6.464,4.005C5.554,4.73,4.73,5.554,4.005,6.464l1.994,4.192c-0.347,0.648-0.632,1.334-0.849,2.05l-4.378,1.557C0.708,14.834,0.667,15.412,0.667,16s0.041,1.165,0.105,1.736l4.378,1.558c0.217,0.715,0.502,1.401,0.849,2.049l-1.994,4.193c0.725,0.909,1.549,1.733,2.459,2.458l4.192-1.993c0.648,0.347,1.334,0.633,2.05,0.851l1.557,4.377c0.571,0.064,1.148,0.104,1.737,0.104c0.588,0,1.165-0.04,1.736-0.104l1.558-4.377c0.715-0.218,1.399-0.504,2.049-0.851l4.193,1.993c0.909-0.725,1.733-1.549,2.458-2.458l-1.993-4.193c0.347-0.647,0.633-1.334,0.851-2.049L31.229,17.736zM16,20.871c-2.69,0-4.872-2.182-4.872-4.871c0-2.69,2.182-4.872,4.872-4.872c2.689,0,4.871,2.182,4.871,4.872C20.871,18.689,18.689,20.871,16,20.871z';

// Default SVG Styles
DispEntity.DefaultStyle = {
    shape : {
        normal   : { fill:"lightgray", stroke:"lightgray", "stroke-width":"1", cursor:"pointer", shadow: true  }, 
        selected : { fill:"green", stroke:"green", "stroke-width":"2", cursor:"pointer", shadow: true },
        hoverover: { fill:"lightgreen", stroke:"green", "stroke-width":"2", cursor:"pointer", shadow: true },
        inflow   : { fill:"green", stroke:"green", "stroke-width":"2", shadow: true } 
    },
    text  : {
        normal   : { fill:"green", cursor:"pointer", "font-size" : "14", "text-anchor" : "start" }, 
        selected : { fill:"white", cursor:"pointer", "font-size" : "14", "text-anchor" : "start" },
        hoverover: { fill:"green", cursor:"pointer", "font-size" : "14", "text-anchor" : "start" },
        inflow   : { fill:"white", cursor:"pointer", "font-size" : "14", "text-anchor" : "start" }
    },
    icon  : {
        normal   : { fill:"lightgray", stroke:"green", "stroke-width":"1", "cursor":"pointer", path: DispEntity.DEFAULT_ICON }, 
        selected : { fill:"green", stroke:"white", "stroke-width":"2", "cursor":"pointer", path: DispEntity.DEFAULT_ICON },
        hoverover: { fill:"lightgreen", stroke:"green", "stroke-width":"2", "cursor":"pointer", path: DispEntity.DEFAULT_ICON },
        inflow   : { fill:"green", stroke:"white", "stroke-width":"2", "cursor":"pointer", path: DispEntity.DEFAULT_ICON}
    }
};

/**
 * DispEntity class for displaying Entitiy objects 
 * @param Entity       en  
 * @param Rapheael     paper
 * @param SVG attrs    style, see DispEntity.DefaultStyle as example
 * @param function     onClick
 * @returns {DispEntity}
 */
function DispEntity( en, paper, style, onClick ) {
    this.en = en;
    this.paper = paper;
    
    if ( style != null && en != null && en.hasOwnProperty("type") && style.hasOwnProperty(en.type) ) {
       this.style = style[ en.type ];
       deepAttrsMerge(this.style, DispEntity.DefaultStyle);
    }
    else {
        this.style = DispEntity.DefaultStyle;
    }
       
    this.state = DispEntity.NORMAL;
    this.prevState = DispEntity.NORMAL;
    this.isHoverOver = false;
    this.onClickFwd = onClick;
    this.shape = null;
    this.icon = null;
    this.text = null;
    this.u = Dygram.u;
    this.textOffset = { x : 1.5 * this.u, y : this.u }; 
    this.iconOffset = { x : 0, y : 4, width : 10 };
    this.box  = new Box(0, 0, DispEntity.DEFAULT_BOX_WIDTH, DispEntity.DEFAULT_BOX_HEIGHT);
//    var pos = this.en == null ? 0 : this.en.position;
//    var lev = this.en == null ? 0 : this.en.level;
//    this.rectX = this.box.x + pos * (this.box.width + this.u);
//    this.rectY = this.box.y + lev * (this.box.height + 3* this.u);  
}

DispEntity.prototype.toString = function() {
    return "[" + this.en.eId + " DispEntity]";
};

/**
 * Main method to prepare raphael elements for displaying entity. 
 * @param {int} x Initial x xoord of Disp Entity
 * @param {int} y Initial y coord of Disp Entity
 * @returns {unresolved} covering box
 */
DispEntity.prototype.prepareForDisplay = function(x, y) {
    this.box.x = x; this.box.y = y;
    this.prepareShape();
    this.prepareIcon();
    this.prepareText();
    return this.box;
};

DispEntity.prototype.repositionEntity = function(x, y) {
    if ( this.shape != null ) {
       this.reposition(x, y, this.shape);
       this.reposition(x + this.textOffset.x, y + this.textOffset.y, this.text);
       this.repositionIcon(x, y);
       this.box.x = x; this.box.y = y;
    }
};

DispEntity.prototype.reposition = function(x, y, obj) {
    if ( obj != null ) {
        var attrs = obj.attr();
        attrs.x = x; attrs.y = y;
        obj.attr(attrs);
        obj.toFront();
    }
    else console.log("WARNING: Unknown obj");
};

DispEntity.prototype.attachListeners = function(obj, onClickForward) {
    var de = this;
    var onHoverIn = function() {
        var hEn = this.data(DispEntity.EN_REF);
        hEn.shape.attr(hEn.style.shape[DispEntity.HOVER_OVER]); 
        if ( hEn.icon != null )
           hEn.icon.attr(hEn.style.icon[DispEntity.HOVER_OVER]); 
        hEn.text.attr(hEn.style.text[DispEntity.HOVER_OVER]);    
    },
    
    onHoverOut = function() {
        var hEn = this.data(DispEntity.EN_REF);    
        hEn.shape.attr(hEn.style.shape[hEn.state]); 
        if ( hEn.icon != null ) 
           hEn.icon.attr(hEn.style.icon[hEn.state]); 
        hEn.text.attr(hEn.style.text[hEn.state]); 
//        hEn.shape.animate(hEn.style.shape[hEn.state], 10); 
//        if ( hEn.icon != null ) 
//           hEn.icon.animate(hEn.style.icon[hEn.state], 10); 
//        hEn.text.animate(hEn.style.text[hEn.state], 10); 
    },
            
    onClick = function() {
        onClickForward(de);
    };
    
    obj.click(onClick);
    obj.hover(onHoverIn, onHoverOut);
};

DispEntity.prototype.prepareShape = function() {
    this.shape = this.paper.rect( this.box.x, this.box.y, this.box.width, this.box.height);  
    var attrs = this.style.shape[this.state];

    if( this.style.shape[this.state].hasOwnProperty("shadow") && this.style.shape[this.state].shadow ) 
        this.shape.attr(attrs).shadow();
    else
        this.shape.attr(attrs);
    this.addDescription(this.shape);
    this.shape.data(DispEntity.EN_REF, this);
    this.attachListeners(this.shape, this.onClickFwd);
};

DispEntity.prototype.prepareIcon = function() {
    var iconPath = this.style.icon[this.state].path;

    this.icon = this.paper.path(iconPath);
    var attrs = this.style.icon[this.state];
    this.icon.attr(attrs);
    this.addDescription(this.icon);
    this.icon["origBBox"] = this.icon.getBBox();

    this.repositionIcon(this.box.x, this.box.y);

    this.icon.data(DispEntity.EN_REF, this);
    this.attachListeners(this.icon, this.onClickFwd);
};

DispEntity.prototype.prepareText = function() {  
    var n = this.en.hasOwnProperty("name") ? this.en.name : this.en.eId;
    this.text = this.paper.text(this.box.x + this.textOffset.x, this.box.y + this.textOffset.y, n);
    var attrs = this.style.text[this.state];
    this.text.attr(attrs);
    this.addDescription(this.text);

    splitText(this.shape.getBBox(), this.text);

    this.text.data(DispEntity.EN_REF, this);    
    this.attachListeners(this.text, this.onClickFwd);
};

DispEntity.prototype.repositionIcon = function(x, y) {
    var scale = (2* this.u - this.iconOffset.width * 2)/ this.icon.origBBox.width;
    this.icon.transform("s"+scale+"T"+(x+this.iconOffset.x)+","+(y+this.iconOffset.y));     
    this.icon.toFront();
};
    
DispEntity.prototype.addDescription = function(obj) {
    var n = this.en.hasOwnProperty("name") ? this.en.name : this.en.eId;
    var d = this.en.hasOwnProperty("description") ? n + "\n\n" + this.en.description : n;
    var attrs = obj.attr();
    attrs["title"] = d;     
    obj.attr(attrs);
    
    obj.node.setAttribute('id', "dyg-" + this.en.eId);
    obj.node.setAttribute('data-title', n);
    obj.node.setAttribute('data-content', d);
};

DispEntity.prototype.getDepInPoint = function() {
    return { x : this.box.x + this.box.width / 2,
             y : this.box.y };
};

DispEntity.prototype.getDepOutPoint = function() {
    return { x : this.box.x + this.box.width / 2,
             y : this.box.y + this.box.height };
};

// it is bases for overloading by inhereted classes
DispEntity.prototype.getPathSegmentOut = function(list) { list.pop(); return ""; };

// it is bases for overloading by inhereted classes
DispEntity.prototype.getPathSegmentIn = function(list) { list.pop(); return ""; };

DispEntity.prototype.saveState = function() {
};

DispEntity.prototype.restoreState = function() {
//    if ( this.prevState != this.state ) {
//        console.log(this + " restoring state from " + this.prevState + " to " + this.state);
//        this.state = this.prevState;
//        this.setState(this.state);
//    }
        this.setState(DispEntity.NORMAL);
};

DispEntity.prototype.setState = function(state) {
    console.log(this + " setting state to " + state);
    this.state = state;
    with(this) {
        if ( shape != null ) { 
             
//            if( style.shape[state].hasOwnProperty("shadow") && style.shape[state].shadow ) 
//                shape.attr(style.shape[state]).shadow();
//            else
                shape.attr(style.shape[state]);
            
            addDescription(shape); 
        }
        if ( icon != null ) { icon.attr(style.icon[state]); addDescription(icon); }
        if ( text != null ) { text.attr(style.text[state]); addDescription(text); }
    }
};

DispEntity.prototype.hide = function() {
    with(this) {
        if ( shape != null ) { shape.hide(); }
        if ( icon != null ) { icon.hide(); }
        if ( text != null ) { text.hide(); }
    }    
};

DispEntity.prototype.show = function() {
    with(this) {
        if ( shape != null ) { shape.show(); }
        if ( icon != null ) { icon.show(); }
        if ( text != null ) { text.show(); }
    }    
};
