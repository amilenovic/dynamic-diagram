/* 
 * (c) Biformatic
 * Aleksandar Milenovic
 */

Port.HEIGHT = Dygram.u;
Port.WIDTH  = ~~ (Dygram.u / 2);
Port.CIRCLE_RADIUS = 8;

// Default SVG Styles
Port.DefaultStyle = {
    text  : {
        normal   : { "fill":"black", "font-size" : "11", "text-anchor" : "start" }, 
        selected : { "fill":"green", "font-size" : "11", "text-anchor" : "start" },
        inflow   : { "fill":"green", "font-size" : "11", "text-anchor" : "start" }
    }
};

function Port(port, paper, style, onClick) {
    if ( style != null ) {
       if( style.hasOwnProperty(port.type) ) 
          deepAttrsMerge(style[port.type], Port.DefaultStyle);
       else
          style[port.type] = Port.DefaultStyle;
    }    
    this.superclass(port, paper, style, onClick);
    this.box.height = Port.HEIGHT;
    var r = Port.CIRCLE_RADIUS;
    this.portPath = ["M", this.box.x, this.box.y + Port.HEIGHT, 
               "l", 0, -4,
               "q", -r, 0, -r, -r,
               "q",  0, -r, r, -r,
               "q",  r, 0, r, r,
               "q",  0, r, -r, r ];
}

Port.prototype = new DispEntity();
Port.prototype.superclass = DispEntity;
Port.prototype.constructor = Port;

Port.prototype.toString = function() {
    var name = this.en == null ? "GRID" : this.en.eId;
    return "[" + name + " " + this.box + " Port]";
};

Port.prototype.prepareShape = function() {
    with(this) {
        shape = paper.path(portPath);

        var attrs = style.shape[state];
        shape.attr(attrs).shadow();
        addDescription(shape);
        shape.data(DispEntity.EN_REF, this);
        attachListeners(shape, onClickFwd);
    }
};

Port.prototype.prepareIcon = function() {
};

Port.prototype.prepareText = function() {  
    with(this) {
        var n = en.hasOwnProperty("name") ? en.name : en.eId;
        this.text = paper.text(box.x + Port.WIDTH, box.y, n);
        var attrs = style.text[state];
        text.attr(attrs);
        addDescription(text);
        
        var b = {x: box.x + Port.WIDTH, y: box.y, 
                   x2: box.x + box.width, 
                   y2: box.y + Port.HEIGHT,
                   width: box.width - Port.WIDTH, height: Port.HEIGHT};
        splitText(b, text);
               
        text.data(DispEntity.EN_REF, this); 
        text.hide();
        attachListeners(text, onClickFwd);
    }
};


Port.prototype.repositionEntity = function(x, y) {
    this.box.x = x; this.box.y = y;
//    this.reposition(x, y + Port.CIRCLE_RADIUS, this.shape);
    
    var attrs = this.shape.attr();
    this.portPath[1] = x; this.portPath[2] = y + Port.HEIGHT;
    attrs["path"] = this.portPath;
    this.shape.attr(attrs);
    this.shape.toFront();

    this.reposition(x + Port.WIDTH, y, this.text);
};

Port.prototype.getDepInPoint = function() {
    return { x : this.box.x,
             y : this.box.y };
};

Port.prototype.setState = function(state) {
    this.state = state;
    with(this) {
        shape.attr(style.shape[state]); addDescription(shape);
        text.attr(style.text[state]); addDescription(text);
        if ( state == DispEntity.IN_FLOW ) {
            text.show();
        }
        else {
            text.hide();
        }
    }
};
