/* 
 * Dygram Utility functions
 * 2012/10/29   Aleksandar Milenovic
 */

function splitText(enBBox, txtRect) {
    if ( txtRect.getBBox().x2 < enBBox.x2 )
        return;
    
    // try with space-as-separator convention first
    var fullTxt = txtRect.attr("text"); 
    var splits = fullTxt.split(' ');
    if ( splits.length > 0 ) {
        if ( canFit(txtRect, enBBox, splits[0]) ) {
            var txt = splits[0];
            for( var i = 1; i < splits.length; i++ ) {
                if ( ! canFit(txtRect, enBBox, txt + ' ' + splits[i]) ) {
                    insertBreakAt(txtRect, fullTxt, txt.length+1);
                    break;
                }
                txt += ' ' + splits[i];
            }
        }
        else {
            var pos = findWhereToBreak(txtRect, enBBox, splits[0]);
            insertBreakAt(txtRect, fullTxt, pos);
        }
    }
    
    if ( !canFit(txtRect, enBBox, null) ) {
        cutOff(txtRect, enBBox);
    }
}

function canFit(txtRect, enBBox, proposedTxt) {
    if ( proposedTxt !== null )
       txtRect.attr("text", proposedTxt);
    return txtRect.getBBox().x2 <= enBBox.x2;
}

function insertBreakAt(txtRect, fullTxt, position ) {
    var retTxt = fullTxt.substr(0, position) + '\n' + fullTxt.substr(position);
    txtRect.attr("text", retTxt);
}

function findWhereToBreak(txtRect, enBBox, txt) {
    var start = 0, end = txt.length;
    var mid = ~~((end + start)/2);
    var res = 0;
    while(res !== mid) {
        res = mid;
        if ( canFit(txtRect, enBBox, txt.substr(0, mid)) ) {
            start = mid; mid = ~~((mid + end)/2);
        }
        else {
            end = mid; mid = ~~((start + mid)/2);
        }
    } 
    return res-1;
}

function cutOff(txtRect, enBBox ) {
    var txt = txtRect.attr("text");
    var start = txt.indexOf('\n'), end = txt.length;
    if ( start < 0 ) start = 0;
    
    var mid = ~~((end + start)/2);
    var res = 0;
    while(res !== mid) {
        res = mid;
        if ( canFit(txtRect, enBBox, txt.substr(0, mid) + "...") ) {
            start = mid; mid = ~~((mid + end)/2);
        }
        else {
            end = mid; mid = ~~((start + mid)/2);
        }
    } 
    
    txt = txt.substr(0, mid-1) + "...";
    txtRect.attr("text", txt);
}

function deepAttrsMerge(targetObj, sourceObj) {
    for( var sAttr in sourceObj ) {
        if ( ! targetObj.hasOwnProperty(sAttr) ) { 
            targetObj[sAttr] = sourceObj[sAttr];
        }
        else if ( typeof sourceObj[sAttr] == "object" ) {
            deepAttrsMerge( targetObj[sAttr], sourceObj[sAttr]);
        }
    }
};