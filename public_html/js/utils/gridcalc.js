/* 
 * (c) Biformatic
 * Aleksandar Milenovic
 */

function Box(x, y, w, h) {
    this.x = x;
    this.y = y;
    this.width = w;
    this.height = h;
    
    this.toString = function() {
        return "B[" + this.x + ", " + this.y + ", " + this.width + ", " + this.height + "]";
    };
}

function GridCalc(startX, startY, padX, padY) {
    var levelHeights = {};
    var posWidths = {};
    this.startX = startX;
    this.startY = startY;
    this.padX = padX;
    this.padY = padY;
    this.totalBox = null;
    
    this.addBox = function( box, level, position ) {
        this.updateLevelHeights(level, box);
        this.updatePosWidths(position, box);
    };
    
    this.updateLevelHeights = function( level, box)  {
        if ( levelHeights.hasOwnProperty(level) ) {
            if( levelHeights[level] < box.height ) {
                levelHeights[level] = box.height;
            }
        }
        else {
            levelHeights[level] = box.height;
        }
    };
    
    this.updatePosWidths = function( position, box ) {
        if ( posWidths.hasOwnProperty(position) ) {
            if( posWidths[position] < box.width ) {
                posWidths[position] = box.width;
            }
        }
        else {
            posWidths[position] = box.width;
        }
    };
    
    this.getTotalGridSize = function() {
        var totW = 0, totH = 0, countW = 0, countH = 0;
        for( var w in posWidths ) {
            totW += posWidths[w];
            countW++;
        }
        for( var h in levelHeights) {
            totH += levelHeights[h];
            countH++;
        }
        
        if( countW > 0 ) totW += (countW - 1) * this.padX;
        if( countH > 0 ) totH += (countH - 1) * this.padY;
        this.totalBox = new Box(this.startX, this.startY, totW, totH);
        return this.totalBox;
    };
    
    this.getCoords = function( level, position ) {
        var x = this.startX, y = this.startY, countW = 0, countH = 0;
        for( var i = 0; i < position; i++ ) {
           if ( posWidths.hasOwnProperty(i) ) { 
               x += posWidths[i]; countW++;
           }
        }
        
        for( var j = 0; j < level; j++ ) {
           if ( levelHeights.hasOwnProperty(j) ) {
               y += levelHeights[j]; countH++;
           }
        }
       
        if( countW > 0 ) x += (countW * this.padX);
        if( countH > 0 ) y += (countH * this.padY);
//        console.log("x=" + x + ", y=" + y + ", countW=" + countW + ", countH=" + countH);
        return { "x": x, "y": y, "width": posWidths[position], "height": levelHeights[level] };
    };
    
    /**
     * This segment is totally within this grid.
     * @param {x,y} startP - starting point
     * @param {x,y} endP - ending point
     * @returns SVG path array
     */
    this.getPathSegment = function(startP, endP) {
        var x = this.startX;
        var horizStartOffset, samePosNeighLevels = false; 
        
        for( var i = 0; i < Object.keys(posWidths).length; i++ ) {
           if ( posWidths.hasOwnProperty(i) ) { 
               var nextX = x + posWidths[i] + this.padX; 
               if ( startP.x > x && startP.x < nextX ) {
                   horizStartOffset = { left: x - this.padX / 2 - startP.x, right: nextX - this.padX / 2 - startP.x }; 
                   // check if they are in the same position but neighbouring levels
                   samePosNeighLevels = ( endP.x > x && endP.x < nextX && (startP.y + this.padY) == endP.y );
                   break;
               }
               x = nextX;
           }
        }        
       
        var path;
        if ( startP.x == endP.x && endP.y - startP.y == this.padY )
            return ["M", startP.x, startP.y, "L", endP.x, endP.y];
        else if ( samePosNeighLevels ) {
            path = [ "M", startP.x, startP.y, "l", 0, this.padY / 2, "L", endP.x, endP.y - this.padY / 2,
                     "L", endP.x, endP.y ];
        }
        else {
            // directions, assuming startP.y < endP.y
            var dirStX = startP.x > endP.x ? "left" : "right";

            path = [ "M", startP.x, startP.y,
                         "l", 0, this.padY / 2,
                         "l", horizStartOffset[dirStX], 0,
                         "l", 0, endP.y - startP.y - this.padY,
                         "L", endP.x, endP.y - this.padY / 2,
                         "L", endP.x, endP.y ];
        }
        return path;
    };
    
    /**
     * This segment starts in this Grid and continues in another.
     * @param {x,y} startP  - starting point in this grid.
     * @param {x,y} endP - point in another grid.
     * @returns SVG path array
     */
    this.getPathSegmentOut = function(startP, endP) {
        var x = this.startX;
        var e = endP, lastE = endP;
        // finds exit point first
        for( var i = 0; i < Object.keys(posWidths).length; i++ ) {
           if ( posWidths.hasOwnProperty(i) ) { 
               var posStart = x;
               var posEnd = x + posWidths[i];
               var nextPosStart = posEnd + this.padX; 
               if ( endP.x > posEnd && endP.x < nextPosStart ) {
                   e = { x: (nextPosStart + posEnd)/2, y: this.startY + this.totalBox.height };
                   break;
               }
               else if ( endP.x > posStart && endP.x < posEnd ) {
                   e = startP.x < endP.x 
                       ? lastE 
                       : { x: (nextPosStart + posEnd)/2, y: this.startY + this.totalBox.height };
                   break;
               }
               lastE = { x: (nextPosStart + posEnd)/2, y: this.startY + this.totalBox.height };
               x = nextPosStart;
           }
        }         
        
        var hPadY = this.padY / 2;
        if ( startP.y === (endP.y - this.padY) ) {
            return ["M", startP.x, startP.y, "l", 0, hPadY, "L", endP.x, endP.y - hPadY, "L", endP.x, endP.y ];
        }
        
        var path = [ "M", startP.x, startP.y, 
                     "l", 0, hPadY,
                     "L", e.x, startP.y + hPadY,
                     "L", e.x, e.y, 
                     "l", 0, hPadY, 
                     "L", endP.x, endP.y - hPadY,
                     "L", endP.x, endP.y ];
        return path;
    };
    
    /**
     * This segments starts outside of this Grid and ends inside.
     * @param {x,y} startP - starting point outside of this grid
     * @param {x,y} endP - ending point within this grid
     * @returns SVG path array
     */
    this.getPathSegmentIn = function(startP, endP) {
        var x = this.startX;
        var s = startP, lastS = s;
        var path;
        // finds exit point first
        for( var i = 0; i < Object.keys(posWidths).length; i++ ) {
           if ( posWidths.hasOwnProperty(i) ) { 
               var posStart = x;
               var posEnd = x + posWidths[i];
               var nextPosStart = posEnd + this.padX; 
               if ( startP.x > posEnd && startP.x < nextPosStart ) {
                   s = { x: (nextPosStart + posEnd)/2, y: this.startY };
                   break;
               }
               else if ( startP.x > posStart && startP.x < posEnd ) {
                   s = startP.x >= endP.x ? lastS : { x: (nextPosStart + posEnd)/2, y: this.startY };
                   break;
               }
               lastS = { x: (nextPosStart + posEnd)/2, y: this.startY };
               x = nextPosStart;
           }
        }         
        
        var hPadY = this.padY/2;
        path = [ "M", startP.x, startP.y,
                  "L", startP.x, s.y - hPadY,
                  "L", s.x, s.y - hPadY, 
                  "L", s.x, endP.y - hPadY,
                  "L", endP.x, endP.y - hPadY,
                  "L", endP.x, endP.y ];
        return path;
    };    
}
