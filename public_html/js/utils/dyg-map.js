/* 
 * (c) Biformatic
 * Aleksandar Milenovic
 */

function DispEntityMap() {
    this.entities = {};
    this.parents  = {};
    this.connections = {};
    this.flows = {};
};

DispEntityMap.prototype.toString = function() {
    return "E:" + Object.keys(this.entities) + ", C:" + Object.keys(this.connections);  
};

DispEntityMap.prototype.addDispEntity = function(dispE, parent) {
    this.entities[dispE.en.eId] = dispE;

    // we assume disp entity is added only once, hence no parent set yet
    // we also assume that parent has not been registered yet
    this.parents[dispE.en.eId]  = parent;
};
    
DispEntityMap.prototype.addDispConnection = function(dispC) {
   var cId; 
   if ( dispC.hasOwnProperty("cId") ) {
       cId = dispC;
   }  
   else {
       cId = dispC.fromDispEn.en.eId + "-" + dispC.toDispEn.en.eId;
   }

   this.connections[cId] = dispC;
//   console.log("addDispConnection: " + cId + ", " + this.connections[cId] + " " + this);    
};

DispEntityMap.prototype.addFlow = function(flow) {
//   console.log("addFlow: " + flow.flowConn.cId + ": " + flow + " " + this);    
   this.flows[ flow.flowConn.cId ] = flow; 
};

DispEntityMap.prototype.getDispEntityByEId = function(eId) {
    return this.entities[eId];
};

DispEntityMap.prototype.getDispConnectionByCId = function(cId) {
    var con = this.connections[cId];
//    console.log("getDispConnectionByCId: " + cId + ": " + con + " " + this);
    return this.connections[cId];
};

DispEntityMap.prototype.getFlowByCId = function(cId) {
    return this.flows[cId];  
};
    
DispEntityMap.prototype.getOverArchContainer = function( idA, idB ) {
    var parentsA = [ this.entities[idA] ];
    var parentsB = [ this.entities[idB] ];
    var pA = this.parents[idA], pB = this.parents[idB], c = null;

    do {
        if ( pA == pB ) {
            c = pA;
            break;
        }

        if ( pA.en != null ) {
            parentsA.push(pA);
            pA = this.parents[pA.en.eId];

            if ( parentsB.indexOf( pA ) >= 0) {
                while ( parentsB.pop() != pA );
                c = pA; break;
            }
        }

        if ( pB.en != null ) {
            parentsB.push(pB);
            pB = this.parents[pB.en.eId];

            if ( parentsA.indexOf( pB ) >= 0) {
                while( parentsA.pop() != pB );
                c = pB; break;
            }
        }

    } while ( true );

    return { container: c, fromList: parentsA, toList: parentsB };
};
