/* 
 * Raphael function to draw a dygram (Dynamic diagram)
 * 2013/12/09, First release   Aleksandar Milenovic
 */
 
Raphael.fn.drawDygram = function(dygId, selId, flowId) {
 
    console.log("enter drawDygram, dygId=" + dygId + ", selId=" + selId);
    
    this.selected = null;
    this.dygId = dygId;
    this.dyg_pfx = "#dyg" + dygId + "-";
    this.jQ = function(selector) {
        return $(this.dyg_pfx + selector);
    }
    
    this.tabQ = function(tabId) {
        console.log('#dyg-tab' + this.dygId + ' a[href="#dyg' + this.dygId + '-' + tabId + '"]');
        return $('#dyg' + this.dygId + '-tab a[href="#dyg' + this.dygId + '-' + tabId + '"]');
    }
   
    var u = Dygram.u;
    var startX = u, startY = u, width = 12*u, hight = 2*u, round = u;
    var indentXSpace = u, indentYSpace = u/2, colSize = 21*u;    
 
    this.onclick = (function(t) { return function(tEn) {
        var sEn = tEn.en;
        lastSelectedPaper = t;

        if ( t.selected != null ) {
            t.selected.setState( DispEntity.NORMAL );
        }
        tEn.setState( DispEntity.SELECTED );
        t.selected = tEn;
        
        t.tabQ("sel-tab").tab('show');
        
        t.jQ('sel-info').text(sEn.type + " " + sEn.name);
        t.jQ('sel-actions').empty();
        t.jQ('sel-desc').empty();
        if( sEn.hasOwnProperty("description")) { t.jQ('sel-desc').text(sEn.description);}
        if ( dActions != null ) {
            if ( dActions.hasOwnProperty(sEn.type)) {
                var type = dActions[sEn.type];
                
                for(var j = 0; j < type.length; j++) {
                   var action = type[j];
                   t.jQ('sel-actions').append('<li><a onclick="redraw(\'/entity/'+sEn.eId+'/'+action.action+'\')" href="javascript:void(0);">'+action.name+'</a></li>');
                }
            }
        }
        
        t.jQ('sel-prop-list').empty();
        if( sEn.hasOwnProperty("properties") ) {
            for( i = 0, ii = sEn.properties.length; i< ii; i++ ) {
                var prop = sEn.properties[ i ];
                var val = prop.val;
                if ( prop.hasOwnProperty("type") && prop.type.localeCompare("url") == 0 ) {
                    var url = val;
                    if( url.lastIndexOf("http") < 0 ) {
                        url = "http://" + url;
                    }
                    val = '<a onclick="window.open(\''+ url + '\')" href="javascript:void(0);">' + val + '</a>';
                }
                t.jQ('sel-prop-list').append('<tr><td>' + prop.name + '</td><td>' + val + '</td></tr>');
            }
        }
    }})(this);
            
    this.jQ("title").text(this.dygram.title);
    this.jQ("desc").text(this.dygram.description);
    this.tabQ('home-tab').tab('show');
    
    if( this.dygram.hasOwnProperty("entities") && !this.dygram.entities.hasOwnProperty("length") ) {
        this.dygram.entities = [this.dygram.entities];
    }

    this.repositonAll = function() {
        var box = this.groupE.repositionEntity(this.startX, this.startY);
        this.setSize(box.width + u + u, box.height + u + u);
    };    

    console.log(">>>> start drawing entities");

    this.startX = u + u; 
    this.startY = u;
    this.map = new DispEntityMap();
    // Draw Entities 
    
    this.groupE = new DispGroup(null, this.dygram.entities, this.map, this, this.dygram.styles, this.onclick );
    console.log("---->>>> groupE.prepareForDisplay");    
    var box = this.groupE.prepareForDisplay(this.startX, this.startY);
    console.log("---->>>> setSize");    
    this.setSize(box.width + u + u, box.height);
        
//    $("dyg-ent-module").popover('show');
    console.log("<<<< end drawing entities");

    
    console.log("start drawing connection");
    if( this.dygram.hasOwnProperty("connections") ) { 
        console.log("Drawing connections");
        if ( !this.dygram.connections.hasOwnProperty("length") )
            this.dygram.connections = [this.dygram.connections];
        
        for( var i = 0, ii = this.dygram.connections.length; i < ii; i++) {
            var con = this.dygram.connections[i];
            if ( con.hasOwnProperty("from") && con.hasOwnProperty("to")) {
                var fromDE = this.map.getDispEntityByEId( con.from );
                var toDE   = this.map.getDispEntityByEId( con.to );
                var dispC = new DispConnection( fromDE, toDE, this, this.dygram.styles, this.onclick );
                this.map.addDispConnection(dispC);
                
                var c = this.map.getOverArchContainer( fromDE.en.eId, toDE.en.eId );
//                console.log(fromDE.en.eId + "--->" + toDE.en.eId);
//                console.log(c.container + " From:" + c.fromList + " To:" + c.toList);
                
                fromDE = c.fromList.pop();
                toDE   = c.toList.pop();

                var path = fromDE.getPathSegmentOut(c.fromList, dispC);

                var startP = fromDE.getDepOutPoint(c.fromList);
                var endP   = toDE.getDepInPoint(c.toList);                
                path += c.container.grid.getPathSegment(startP, endP);
                
                path += toDE.getPathSegmentIn(c.toList, dispC);
 
//                console.log(path);
                dispC.prepareForDisplay( path );
            }
        }   
    }
    
    console.log("end drawing connection");
    console.log("start drawing groups");
    
    if( this.dygram.hasOwnProperty("groups") ) { 
        console.log("Processing groups.");
        if ( this.dygram.groups.hasOwnProperty("levels") )
            for( var i = 0, ii = this.dygram.groups.levels.length; i < ii; i++) {
                var g = this.dygram.groups.levels[i];        
                var levelB = this.groupE.grid.getCoords( g.from, 0);
                levelB.width = this.groupE.box.width;
                if ( g.from != g.to ) {
                    for( var j = parseInt(g.from) + 1; j <= g.to; j++  ) {
                        var b = this.groupE.grid.getCoords( j, 0);
                        levelB.height += u + u + b.height;
                    }
                }
                var r = this.rect(levelB.x - u - u, levelB.y -u, 
                           levelB.width + u, levelB.height + u + u, 10);
                r.attr( {"fill":"white", "stroke":"lightgray", "stroke-width":"1"} );
                r.toBack();
                
                var t = this.text(levelB.x - u - u/2, levelB.y - u/2, g.name);
                t.attr( {"text-anchor" : "start", "fill": "green"} );
            }
    }    
    
    console.log("stop drawing groups");
    console.log("start drawing flows");
    
    this.selFlow = [];
    
    
    if( this.dygram.hasOwnProperty("flows") ) { 
        console.log("Processing flows.");

        if ( !this.dygram.flows.hasOwnProperty("length") )
            this.dygram.flows = [this.dygram.flows];        
        
        this.selFlow = this.dygram.flows[0];
        this.jQ("flow-title").text(this.selFlow.flow);
        this.jQ("flow-info").text(this.selFlow.description);
        this.jQ('sel-flow-list').empty();
               
        this.flowConPos = 0;       
        
        this.nextFlowStep = (function(t) { return function() {
            var prevSelCon = t.map.getFlowByCId(t.selFlow.connections[t.flowConPos].cId); 
            var newCon = null;
            t.flowConPos++;
            if( t.flowConPos < t.selFlow.connections.length ) {
                newCon = t.map.getFlowByCId(t.selFlow.connections[t.flowConPos].cId);
            } 
            else {
                t.flowConPos = 0;
                newCon = t.map.getFlowByCId(t.selFlow.connections[0].cId);
            }

            console.log("Prev: " + prevSelCon + ", next: " + newCon);
            prevSelCon.inFlowOff();
            newCon.inFlowOn();
            t.jQ('flow-step-info').text(newCon.flowConn.description);
            t.jQ('flow-steps').text((t.flowConPos + 1) + " of " + t.selFlow.connections.length);
        }; } )(this);

        this.previousFlowStep = (function(t) { return function() {
            var prevSelCon = t.map.getFlowByCId(t.selFlow.connections[t.flowConPos].cId); 
            var newCon = null;        
            t.flowConPos--;
            if( t.flowConPos >= 0 ) {
                newCon = t.map.getFlowByCId(t.selFlow.connections[t.flowConPos].cId);
            } 
            else {
                t.flowConPos = t.selFlow.connections.length - 1; 
                newCon = t.map.getFlowByCId(t.selFlow.connections[t.flowConPos].cId);
            }

            prevSelCon.inFlowOff();
            newCon.inFlowOn();
            t.jQ('flow-step-info').text(newCon.flowConn.description);
            t.jQ('flow-steps').text((t.flowConPos + 1) + " of " + t.selFlow.connections.length);
        }; })(this);        

        this.startFlow = (function(t) { return function() {
            var prevSelCon = t.map.getFlowByCId(t.selFlow.connections[t.flowConPos].cId); 
            t.flowConPos = 0;
            var newCon = t.map.getFlowByCId(t.selFlow.connections[t.flowConPos].cId);
            prevSelCon.inFlowOff();
            newCon.inFlowOn();
            t.jQ('flow-step-info').text(newCon.flowConn.description);
            t.jQ('flow-steps').text((t.flowConPos + 1) + " of " + t.selFlow.connections.length);
            t.jQ('next').prop('disabled', false);
            t.jQ('prev').prop('disabled', false);
        }; })(this);   
    
        this.stopFlow = (function(t) { return function() {
            var prevSelCon = t.map.getFlowByCId(t.selFlow.connections[t.flowConPos].cId); 
            prevSelCon.inFlowOff();
            t.jQ('flow-step-info').text("");
            t.jQ('flow-steps').text("");
            t.jQ('next').prop('disabled', true);
            t.jQ('prev').prop('disabled', true);
        }; })(this); 
    
        this.jQ("next").click( this.nextFlowStep);
        this.jQ("prev").click( this.previousFlowStep);
        this.jQ("start").click( this.startFlow );
        this.jQ("stop").click( this.stopFlow );
        this.jQ('next').prop('disabled', true);
        this.jQ('prev').prop('disabled', true);

        for( var i = 0, ii = this.dygram.flows.length; i < ii; i++) {
            var flow = this.dygram.flows[i];            
            var selFlowAction = (function(t, flow) { return function() {
                t.selFlow = flow;
                t.stopFlow();
                t.jQ("flow-title").text(t.selFlow.flow);
                t.jQ("flow-info").text(t.selFlow.description);                
            }})(this, flow);
            var flowAction = this.jQ('sel-flow-list').append('<li><a href="javascript:void(0);">'+flow.flow+'</a></li>');
            $(flowAction).click(selFlowAction);
            
            for( var j = 0; j < flow.connections.length; j++ ) {
                var flowConn = flow.connections[j];
                // we assume that all flow connections are ordered and hence inSynWith dependencies
                // are also ordered.
                var f = new DispFlow(flowConn, this.map);
                this.map.addFlow(f);
            }
        }
        
        if ( flowId != null ) {
            this.tabQ('flow-tab').tab('show');
            this.startFlow();
        }         
    }    
    
    if ( selId != null ) {
        // find and select the entity with selId
        var s = this.map.getDispEntityByEId(selId);
        this.onclick(s);
    }
    
    console.log("stop drawing flows");
    
    // Fix this not to be a global parameter
    dActions = this.dygram.actions;  
    
    console.log("left drawDygram");
};


