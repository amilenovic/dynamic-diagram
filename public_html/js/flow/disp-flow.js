/* 
 * (c) Biformatic
 * Aleksandar Milenovic
 */

function DispFlow(flowConn, map) {
    this.flowConn = flowConn;
    this.conn = map.getDispConnectionByCId(flowConn.cId);
    this.connInSyncWith = flowConn.hasOwnProperty("inSyncWith") 
         ? map.getFlowByCId( flowConn.inSyncWith )
         : null;
}


DispFlow.prototype.toString = function() {
    return "[" + this.flowConn.cId + ", in sync with " + this.connInSyncWith + " DispFlow]";
};

DispFlow.prototype.inFlowOn = function() {
    console.log("inFlowOn " + this);
    with(this) {
        if ( connInSyncWith != null ) {
            connInSyncWith.inFlowOn();
        }
        conn.saveState();
        conn.setState( DispConnection.IN_FLOW );

        conn.fromDispEn.saveState();
        conn.fromDispEn.setState( DispEntity.IN_FLOW );
        conn.toDispEn.saveState();
        conn.toDispEn.setState( DispEntity.IN_FLOW );
    }
};

DispFlow.prototype.inFlowOff = function() {
    console.log("inFlowOff " + this);
    with(this) {
        if ( connInSyncWith != null ) {
            connInSyncWith.inFlowOff();
        }
        conn.restoreState();
        conn.fromDispEn.restoreState();
        conn.toDispEn.restoreState();
    }    
};
