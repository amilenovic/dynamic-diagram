/* 
 * Startup function that is processing 'dygram' declared HTML elements.
 * Aleksandar Milenovic
 * 2013/12/09, First release   Aleksandar Milenovic
 */

requirejs.config({
    baseUrl: 'js',
    paths: {
        jquery:    'libs/jquery.min',
        raphael:   'libs/raphael-min',
        bootstrap: 'libs/bootstrap.min',
        tooltip:   'libs/tooltip'
    },
    shim: {
        bootstrap: {
            deps: ['jquery']
        },
        tooltip: {
            deps: ['bootstrap']
        },
        "grp/dyg-map": {
            deps: ['ent/disp-entity', 'conn/disp-con']
        },
        "ent/disp-entity": {
            deps: ['utils/dygram-utils', 'utils/fraphael']  
        },
        "ent/disp-ent-ports": {
            deps: ['ent/disp-entity']
        },
        "grp/disp-group": {
            deps: ['ent/disp-entity', 'utils/gridcalc', 'grp/dyg-map']
        },
        "ent/Port": {
            deps: ['ent/disp-entity']
        },
        "grp/sml-group": {
            deps: ['ent/disp-entity']
        },
        "ent/sml-entity": {
            deps: ['ent/disp-entity']
        },
        "dyg-ctrl": {
            deps: ['ent/disp-entity', 'conn/disp-con', 'utils/gridcalc', 'grp/dyg-map']
        },
        "utils/fraphael": {
            deps: ['raphael']
        }
    }
});

require(['jquery', 'raphael', 'bootstrap', 'tooltip', 
         'utils/fraphael', 'utils/gridcalc', 'grp/dyg-map', 'utils/dygram-utils', 
         'ent/disp-entity', 'ent/disp-ent-ports', 'grp/disp-group', 'ent/Port', 'ent/sml-entity', 'grp/sml-group',
         'conn/disp-con', 'flow/disp-flow',
         'dyg-ctrl' ], 
function() {
    console.log("Got in");
    $(document).ready(function() {
        $(".dygram").each(function(index, domEl) {
            console.log("Processing index=" + index);
            var r = Raphael(domEl, "100%", "100%");
            r["unit"] = 10;
            lastSelectedPaper = r;
            var tag = $(this).data("source-tag");
            var selId = $(this).data("selection");
            
            var nb = $(this).data("nav-bar");
            if ( nb ) {
                $(this).prepend('<div class="dyg-nav"></div>'); 
                var nbc = $(this).children(".dyg-nav");
                console.log("Replacing navbar.html for index=" + index);
                nbc.load("navbar.html", function() {
                    // when loaded, execute this:
                    nbc.html(nbc.html().replace(/\{0\}/g, index));
                    console.log("Just replaced navbar.html for index=" + index);
//                    $('#dyg-tab2 a[href="#dyg2-home-tab]"').tab('hide');
                });
            }

            console.log("Drawing dygram for index=" + index + ", selId=" + selId);
            if ( tag != null ) {
//                arrangeDygram($(tag).val());
                r["dygram"] = $.parseJSON( $(tag).val() );
                r.drawDygram(index, selId);
            }
            else {
                var f = $(this).data("source");
                $.getJSON(f, null, function(dygram) {
                    r["dygram"] = dygram;
                    r.drawDygram(index, selId);
                });        
            }
            
            $(this).height(r.realHeight);
            
        });        
    });
});

function Dygram() {
    
};

Dygram.u = 20;    

function arrangeDygram(dygram) {   
//    debug("input dygram: " + dygram);
    var r = lastSelectedPaper;
    if ( r != null ) {
        r["dygram"] = null;
        r.clear();
    }
    else {
        // we should not enter this branch as we rely on onload function
        // to already initialize lastSelectedPaper
        var domEl = $(".dygram"); // assumed ony one dygram
        domEl.empty();
        r = Raphael(domEl, 1200, 600); 
        lastSelectedPaper = r;
    }
    
    r["dygram"] = $.parseJSON(dygram);
    r.drawDygram();
    
//    $.ajax({
//        url: '/Dygram/webresources/group',
//        type: 'POST',
//        data: dygram,
//        contentType: 'application/json',
//        dataType: 'json',
//        async: false,
//        success: function(arrangedDyg) {
//            debug(arrangedDyg);
//            r["dygram"] = arrangedDyg;
//            r.drawDygram();
//        }
//    });    
}

