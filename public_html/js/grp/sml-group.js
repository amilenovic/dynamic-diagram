/* 
 * (c) Biformatic
 * Aleksandar Milenovic
 */

function SmallGroup(en, entities, map, paper, style, onClick) {
    this.superclass(en, paper, style, onClick);

    this.dispEntities = [];
    this.selectors = {};
    
    this.selector = null;
    for( var i = 0; i < entities.length; i++ ) {
        var chE = entities[i];
        var dispE = new SmallEntity( chE, this.paper, this.style, onClick);
        
        if ( !this.selectors.hasOwnProperty(chE.type) ) {
            this.selectors[chE.type] = {
                selector: null, summary: null,
                entities: [], isCollapsed: false
            };
        }
        this.selectors[chE.type].entities.push(dispE);
   
        this.dispEntities.push(dispE);
        map.addDispEntity(dispE, this);
    } 
    
    for( var t in this.selectors ) {
        var s = this.selectors[t];
        if ( s.entities.length > 1 ) {
            s.summary = new SmallEntity( 
                {  
                   type: t, 
                   name: (s.entities.length + " " + t + "s") 
                }, 
                paper, style, onClick 
            );
        }
    }
}

SmallGroup.prototype = new DispEntity();
SmallGroup.prototype.superclass = DispEntity;
SmallGroup.prototype.constructor = SmallGroup;

SmallGroup.prototype.toString = function() {
    var name = this.en == null ? "GRID" : this.en.eId;
    return "[" + name + " SmallGroup]";
};

SmallGroup.prototype.prepareForDisplay = function(x, y) {   
    var offset = { x : this.en == null ? 0 : this.textOffset.x,
                   y : this.en == null ? 0 : DispEntity.DEFAULT_BOX_HEIGHT
    };
    var startX = x + offset.x;
    var startY = y + offset.y;

//    var count = 0;
//    for( var t in this.selectors ) {
//        var s = this.selectors[t];
//        if ( s.entities.length > 1 ) {               
//            this.prepareSelector(t); 
//            s.summary.prepareForDisplay( startX, startY + count * SmallEntity.HEIGHT);
//            count++;
//
//            for( var i = 0; i < s.entities.length; i++ ) {
//                s.entities[i].prepareForDisplay( startX, startY + i * SmallEntity.HEIGHT );
//                s.entities[i].hide();
//            }
//        }
//        else {
//            s.entities[0].prepareForDisplay( startX, startY + SmallEntity.HEIGHT );
//            count++;
//        }   
//    }     
    
    // replace following 4 lines with above comment to get selectors in place.
    var count = this.dispEntities.length;
    for( var i = 0; i < count; i++ ) {
        this.dispEntities[i].prepareForDisplay( startX, startY + i * SmallEntity.HEIGHT );
    }
    
    
    this.box.x = x; this.box.y = y;
    this.box.height += count * SmallEntity.HEIGHT;
    
    if ( this.en != null ) {
       this.superclass.prototype.prepareForDisplay.call(this, x, y); 
    }
    
    return this.box;
};

SmallGroup.prototype.repositionEntity = function(x, y) {
    this.superclass.prototype.repositionEntity.call(this, x, y);
    var offset = { x : this.en == null ? 0 : this.textOffset.x,
                   y : this.en == null ? 0 : DispEntity.DEFAULT_BOX_HEIGHT
    };
    var startX = x + offset.x;
    var startY = y + offset.y;
    
//    var count = 0;
//    for( var t in this.selectors ) {
//        var s = this.selectors[t];
//        if ( s.entities.length > 1  ) {
//            if ( s.isCollapsed ) {
//                s.summary.repositionEntity(startX, startY + count * SmallEntity.HEIGHT);
//                this.repositionSelector(s.selector, x, startY + s.summary.iconOffset.y + count * SmallEntity.HEIGHT);
//                count++;
//            }
//            else {
//                for ( var i = 0; i < s.entities.length; i++ ) {
//                    s.entities[i].repositionEntity(startX, startY + count * SmallEntity.HEIGHT);
//                    count++;
//                }
//            }
//        }   
//        else {
//            s.entities[0].repositionEntity(startX, startY + count * SmallEntity.HEIGHT);
//            count++;            
//        }
//    }
    
    // replace following 4 lines with above comment to get selectors in place.
    var count = this.dispEntities.length;
    for( var i = 0; i < count; i++ ) {
        this.dispEntities[i].repositionEntity(startX, startY + i * SmallEntity.HEIGHT);
    }
    
};

SmallGroup.prototype.prepareSelector = function(t) {
    this.selectors[t].selector = this.paper.path("M11.166,23.963L22.359,17.5c1.43-0.824,1.43-2.175,0-3L11.166,8.037c-1.429-0.826-2.598-0.15-2.598,1.5v12.926C8.568,24.113,9.737,24.789,11.166,23.963z");
    var s = this.selectors[t].selector;
    var attrs = {fill: "white", "stroke-width": "0", cursor: "pointer", title: "Expand/ Collapse"};
    s.attr(attrs);
    s["origBBox"] = s.getBBox();    
    s["sel_type"] = t;
    
    this.attachSelListener(s, this.paper.repositonAll );
    s.toFront();
};

SmallGroup.prototype.repositionSelector = function(s, x, y) {
    var scale = (2* (this.u - 16))/ s.origBBox.width;
    s.transform("s"+scale+"T"+x+","+y);     
    s.toFront();    
};

SmallGroup.prototype.attachSelListener = function(s, repositionAll) {
    var sg = this;
    
    var onClick = function() {
       var t = this.sel_type;
       if ( sg.selectors[t].isCollapsed ) {
           sg.box += sg.selectors[t].entities.length - 1;
           for ( var i = 0; i < sg.selectors[t].entities.length; i++) {
               sg.selectors[t].entities[i].show();
           }
//           sg.selectors[t].summary.hide();
       }
       else {
           sg.box -= sg.selectors[t].entities.length - 1;           
           for ( var i = 0; i < sg.selectors[t].entities.length; i++) {
               sg.selectors[t].entities[i].hide();
           }
//           sg.selectors[t].summary.show();
       }
              
       sg.selectors[t].isCollapsed = ! sg.selectors[t].isCollapsed;
//       repositionAll.call(sg.paper);  
       sg.repositionEntity.call(sg, sg.box.x, sg.box.y);
    };
    
    s.click(onClick);
};
